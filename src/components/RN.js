import React, { useContext } from 'react';
import { AppContext } from '../context/AppContext';

const RN = () => {
  const { value } = useContext(AppContext);
  return (
    <textarea rows="25" cols="80" placeholder={value}/>    
  );
};

export default RN;