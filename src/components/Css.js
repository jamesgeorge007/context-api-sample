import React, { useContext, useState } from 'react';
import { AppContext } from '../context/AppContext';

const Css = () => {
  const { value, setValue } = useContext(AppContext);
  const [content, setContent] = useState("");
  return (
    <>
      <textarea rows="25" cols="80" placeholder={value} onChange={(e) => setContent(e.target.value)}/>
      <button onClick={() => setValue(content)}>Update</button>
    </>
);

};

export default Css;