import React, { useState } from 'react';
import { AppContext } from './context/AppContext';

import Css from './components/Css';
import RN from './components/RN';

function App() {
  const [value, setValue] = useState("Initial Value");

  return (
    <AppContext.Provider
      value={{ value, setValue }}
    >
      <Css />
      <RN />
    </AppContext.Provider>
  );
}

export default App;
